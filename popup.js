

chrome.tabs.onUpdated.addListener(function(tabId, obj) {
  if(obj.status == "complete"){
     executeProd()
  }
});

function executeProd() {

  chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
      var url = tabs[0].url;
      var pattern = /^file:/.test(url) ? url : url.replace(/\/[^\/]*?$/, '/*');
      
      if (pattern === "http://www.apple.com/retail/*"){

        chrome.tabs.executeScript(null, {file: "js/jquery-1.11.3.min.js"}, function(){

          chrome.tabs.executeScript(null, {file: "js/responsiveVoice.js"}, function(){

            chrome.tabs.executeScript(null, {file: "js/beatport.js"}, function(){
            
            });
            
          });       
        });
      }else if(pattern === "https://www.facebook.com/*"){
        chrome.tabs.executeScript(null, {file: "js/jquery-1.11.3.min.js"}, function(){

          chrome.tabs.executeScript(null, {file: "js/responsiveVoice.js"}, function(){

            chrome.tabs.executeScript(null, {file: "js/facebook.js"}, function(){
            
            });
            
          });       
        });
      }else if(pattern === "https://www.youtube.com/*"){

        chrome.tabs.executeScript(null, {file: "js/jquery-1.11.3.min.js"}, function(){

          chrome.tabs.executeScript(null, {file: "js/responsiveVoice.js"}, function(){

            chrome.tabs.executeScript(null, {file: "js/youtube.js"}, function(){
            
            });
            
          });       
        }); 
      }else if(pattern === "https://www.apostar.com.co/*"){

        chrome.tabs.executeScript(null, {file: "js/jquery-1.11.3.min.js"}, function(){

          chrome.tabs.executeScript(null, {file: "js/responsiveVoice.js"}, function(){

            chrome.tabs.executeScript(null, {file: "js/main.js"}, function(){
            
            });
            
          });       
        }); 
      }


  });
}